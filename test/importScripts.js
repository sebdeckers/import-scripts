/* global bar a b */
import test from 'ava'

import requireScripts from '..'

test('import into global context', (t) => {
  requireScripts('fixtures/foo.js')
  t.deepEqual(bar, {lol: 123}, 'import global variable')
})

test('return void', (t) => {
  const result = requireScripts()
  t.is(result, void 0)
})

test('multiple scripts at once', (t) => {
  requireScripts('fixtures/a.js', 'fixtures/b.js')
  t.is(a, 1, 'import first script')
  t.is(b, 2, 'import second script')
})

test('report actual file path', (t) => {
  try {
    requireScripts('fixtures/error.js')
  } catch (error) {
    t.regex(error.stack, /error.js/)
  }
})

test('NetworkError if script can not be loaded', (t) => {
  try {
    requireScripts('this is not a script')
  } catch (error) {
    t.is(error.name, 'NetworkError')
  }
})

test('SyntaxError if url is invalid', (t) => {
  try {
    requireScripts(null)
  } catch (error) {
    t.is(error.name, 'SyntaxError')
  }
})
