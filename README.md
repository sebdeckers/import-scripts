# importScripts

Node.js [ponyfill](https://github.com/sindresorhus/ponyfill) of `importScripts()` from the Web APIs for Service Workers and Web Workers.

## Usage

```js
import importScripts from 'import-scripts'

importScripts('foo.js')

// foo.js:
// var bar = {lol: 123}

console.dir(bar)
// Result: {lol: 123}
```

## Use Case
Useful when writing testing for code that runs in Service Workers.

## References

- [MDN: WorkerGlobalScope.importScripts()](https://developer.mozilla.org/en-US/docs/Web/API/WorkerGlobalScope/importScripts)
- [Web Workers § 4.1. Importing scripts and libraries](https://w3c.github.io/workers/#dom-workerglobalscope-importscripts)

## See Also

- https://github.com/JamesJansson/importScripts

## Colophon

Made by Sebastiaan Deckers in Singapore 🇸🇬
