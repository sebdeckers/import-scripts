import {resolve} from 'path'
import {runInThisContext} from 'vm'
import {readFileSync} from 'fs'

class NetworkError extends Error {
  name = 'NetworkError'
}

export default function importScripts (...scripts) {
  for (const script of scripts) {
    let filepath, code

    try {
      filepath = resolve(script)
    } catch (error) {
      throw new SyntaxError(error.message)
    }

    try {
      code = readFileSync(filepath, 'utf-8')
    } catch (error) {
      throw new NetworkError(error.message)
    }

    runInThisContext(code, {filename: filepath})
  }
}
